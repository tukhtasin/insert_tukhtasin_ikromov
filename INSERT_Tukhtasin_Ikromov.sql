-- Insert the film into the "film" table
INSERT INTO film (title, release_year, rental_duration, rental_rate)
VALUES ('The Shawshank Redemption', 1994, 2, 4.99);

 -- Insert actors into the "actor" table,
INSERT INTO actor (first_name, last_name)
VALUES ('Tim', 'Robbins');

INSERT INTO actor (first_name, last_name)
VALUES ('Morgan', 'Freeman');

INSERT INTO actor (first_name, last_name)
VALUES ('Bob', 'Gunton');

--Insert actors into the "film_actor" table,
INSERT INTO film_actor (film_id, actor_id)
VALUES (1, 1);

INSERT INTO film_actor (film_id, actor_id)
VALUES (1, 2);

INSERT INTO film_actor (film_id, actor_id)
VALUES (1, 3);

INSERT INTO inventory (film_id, store_id)
VALUES (1, 1);

SELECT * FROM actor WHERE actor_id = 1 AND first_name = 'Tim';

SELECT * FROM inventory WHERE film_id = 1 AND store_id = 1;

